const CustomerRouter = require("express").Router();
const { default: mongoose } = require("mongoose");
const { Customer } = require("../models/CustomerModel");
const { Order } = require("../models/OrderModel");
const { Product } = require("../models/ProductModel");

CustomerRouter.post("/customers", async (req, res) => {
  try {
    const customer = new Customer({
      fullName: req.body.fullName,
      phone: req.body.phone,
      email: req.body.email,
      address: req.body.address,
      city: req.body.city,
      country: req.body.country,
      orders: req.body.orders,
    });
    const newCustomer = await customer.save();
    return res.status(201).send(newCustomer);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

CustomerRouter.post("/customers/order", async (req, res) => {
  try {
    const checkPhoneExisted = await Customer.findOne({ phone: req.body.phone });
    if (!checkPhoneExisted) {
      const newCustomer = await Customer.create({
        fullName: req.body.fullName,
        phone: req.body.phone,
        email: req.body.email,
        address: req.body.address,
        city: req.body.city,
        country: req.body.country,
      });
      if (newCustomer) {
        const newOrder = await Order.create({
          orderDate: req.body.orderDate,
          shippedDate: req.body.shippedDate,
          note: req.body.note,
          orderDetail: req.body.orderDetail,
          cost: req.body.cost,
          receiver: req.body.fullName,
          address: req.body.address,
        });
        const customerUpdate = await Customer.findByIdAndUpdate(
          newCustomer._id,
          { $push: { orders: newOrder._id } },
          { new: true }
        ).populate("orders");
        return res.status(201).json(customerUpdate);
      }
    } else {
      const newOrder = await Order.create({
        orderDate: req.body.orderDate,
        shippedDate: req.body.shippedDate,
        note: req.body.note,
        orderDetail: req.body.orderDetail,
        cost: req.body.cost,
        receiver: req.body.fullName,
        address: req.body.address,
      });
      req.body.orderDetail.forEach((product) => {
        Product.findByIdAndUpdate(product.id, {
          $inc: { amount: -product.quantity },
        }).exec();
      });
      req.body.orderDetail.forEach((product) => {
        Product.findByIdAndUpdate(product.id, {
          $push: { orders: newOrder._id },
        }).exec();
      });
      const customerUpdate = await Customer.findByIdAndUpdate(
        checkPhoneExisted._id,
        {
          $push: { orders: newOrder._id },
          fullName: req.body.fullName,
          email: req.body.email,
          address: req.body.address,
          city: req.body.city,
          country: req.body.country,
        },
        { new: true }
      ).populate("orders");
      return res.status(201).json(customerUpdate);
    }
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

CustomerRouter.get("/customers", async (req, res) => {
  try {
    const customerList = await Customer.find().populate("orders");
    return res.status(200).send(customerList);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

CustomerRouter.get("/customers/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Customer ID");
    }

    const customer = await Customer.findById(req.params.id).populate("orders");
    return res.status(200).send(customer);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

//get by phone number
CustomerRouter.get("/customers/phone/:phone", async (req, res) => {
  try {
    const customer = await Customer.findOne({
      phone: req.params.phone,
    }).populate("orders");
    if (!customer) {
      return res.status(404).json({ message: "Customer not found!" });
    } else {
      return res.status(200).json(customer);
    }
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

CustomerRouter.put("/customers/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Customer ID");
    }

    const customerUpdated = await Customer.findByIdAndUpdate(
      req.params.id,
      req.body,
      {
        new: true,
      }
    ).populate("orders");
    return res.status(200).send(customerUpdated);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

CustomerRouter.delete("/customers/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Customer ID");
    }

    await Customer.findByIdAndDelete(req.params.id);
    return res.status(200).json({ message: "Delete successfully!" });
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

module.exports = CustomerRouter;
