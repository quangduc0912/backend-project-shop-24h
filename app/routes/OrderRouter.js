const OrderRouter = require("express").Router();
const { default: mongoose } = require("mongoose");
const { Customer } = require("../models/CustomerModel");
const { Order } = require("../models/OrderModel");
const { Product } = require("../models/ProductModel");

OrderRouter.post("/orders", async (req, res) => {
  try {
    if (req.body.orderDetail) {
      req.body.orderDetail.forEach((product) => {
        Product.findByIdAndUpdate(product.id, {
          $inc: { amount: -product.quantity },
        }).exec();
      });
    }
    const order = new Order({
      orderDate: req.body.orderDate,
      shippedDate: req.body.shippedDate,
      note: req.body.note,
      orderDetail: req.body.orderDetail,
      cost: req.body.cost,
    });
    const newOrder = await order.save();
    req.body.orderDetail.forEach((product) => {
      Product.findByIdAndUpdate(product.id, {
        $push: { orders: newOrder._id },
      }).exec();
    });
    return res.status(201).send(newOrder);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

OrderRouter.get("/orders", async (req, res) => {
  try {
    const ordersList = await Order.find().populate("orderDetail");
    return res.status(200).send(ordersList);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

OrderRouter.get("/orders/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Order ID");
    }
    const order = await Order.findById(req.params.id);
    return res.status(200).send(order);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

OrderRouter.put("/orders/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Order ID");
    }
    const orderUpdated = await Order.findByIdAndUpdate(
      req.params.id,
      req.body,
      {
        new: true,
      }
    );
    return res.status(200).send(orderUpdated);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

OrderRouter.delete("/orders/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Order ID");
    }

    await Order.findByIdAndDelete(req.params.id);
    const orderOfCustomer = await Customer.findOne({ orders: req.params.id });
    if (orderOfCustomer) {
      await Customer.findByIdAndUpdate(orderOfCustomer._id, {
        $pull: { orders: req.params.id },
      });
    }
    return res.status(200).json({ message: "Delete successfully!" });
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

module.exports = OrderRouter;
