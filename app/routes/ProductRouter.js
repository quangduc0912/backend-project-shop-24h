const ProductRouter = require("express").Router();
const { Product } = require("../models/ProductModel");
const { Category } = require("../models/CategoryModel");
const { Platform } = require("../models/PlatformModel");
const { default: mongoose } = require("mongoose");

ProductRouter.post("/products", async (req, res) => {
  try {
    const category = await Category.findById(req.body.category);
    if (!category) return res.status(400).json("Invalid Category");
    const platform = await Platform.findById(req.body.platform);
    if (!platform) return res.status(400).json("Invalid platform");

    const product = new Product({
      name: req.body.name,
      description: req.body.description,
      platform: req.body.platform,
      category: req.body.category,
      imageUrl: req.body.imageUrl,
      buyPrice: req.body.buyPrice,
      promotionPrice: req.body.promotionPrice,
      rate: req.body.rate,
      amount: req.body.amount,
    });

    const newProduct = await product.save();
    return res.status(201).send(newProduct);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/products", async (req, res) => {
  try {
    const limitNumber = req.query.limit ? req.query.limit : 0;
    let { name, maxPrice, platforms, categories, minPrice } = req.query;
    let condition = {};

    if (name) {
      const regex = new RegExp(`${name}`);
      condition.name = regex;
    }

    if (maxPrice) {
      condition.buyPrice = {
        ...condition.buyPrice,
        $lte: maxPrice,
      };
    }

    if (minPrice) {
      condition.buyPrice = {
        ...condition.buyPrice,
        $gte: minPrice,
      };
    }

    if (categories) {
      if (categories.length > 1) {
        filter = categories.split(",");
        condition.category = filter;
      } else {
        condition.category = `${categories}`;
      }
    }

    if (platforms) {
      if (platforms.length > 1) {
        filter = platforms.split(",");
        condition.platform = filter;
      } else {
        condition.platform = `${platforms}`;
      }
    }

    const productsList = await Product.find(condition)
      .populate("category", "name")
      .populate("platform", "name")
      .limit(limitNumber)
      .sort({ timeUpdated: -1 });
    return res.status(200).send(productsList);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/discount", async (req, res) => {
  try {
    const discountProducts = await Product.find().sort({  promotionPrice: -1 })
    return res.status(200).json(discountProducts)
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/priceHighest", async (req, res) => {
  try {
    const discountProducts = await Product.find().sort({  buyPrice: -1 })
    return res.status(200).json(discountProducts)
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/priceLowest", async (req, res) => {
  try {
    const discountProducts = await Product.find().sort({  buyPrice: 1 })
    return res.status(200).json(discountProducts)
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/videos/", async (req, res) => {
  try {
    const videosProducts = await Product.find({
      category: { _id: "62da13097c837a7d3ca28d4a" },
    });
    return res.status(200).json(videosProducts);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/consoles/", async (req, res) => {
  try {
    const consoleProducts = await Product.find({
      category: { _id: "62d981c670db2be556175020" },
    });
    return res.status(200).json(consoleProducts);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/accessorizes/", async (req, res) => {
  try {
    const accessorizesProducts = await Product.find({
      category: { _id: "62da12dd7c837a7d3ca28d48" },
    });
    return res.status(200).json(accessorizesProducts);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.get("/products/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(400).json("Invalid Product ID");
    const product = await Product.findById(req.params.id)
      .populate("category", "name")
      .populate("platform", "name")
      .populate("orders");
    return res.status(200).send(product);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.put("/products/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Product ID");
    }

    if (req.body.category) {
      const category = await Category.findById(req.body.category);
      if (!category) return res.status(404).json("Invalid Category");
    }

    if (req.body.platform) {
      const platform = await Platform.findById(req.body.platform);
      if (!platform) return res.status(404).json("Invalid platform");
    }

    const product = await Product.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    return res.status(200).send(product);
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

ProductRouter.delete("/products/:id", async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json("Invalid Product ID");
    }

    const product = await Product.findByIdAndDelete(req.params.id);
    return res.status(200).json("Deleted Successfully!");
  } catch (err) {
    return res.status(500).json({ message: `${err.message}` });
  }
});

module.exports = ProductRouter;
