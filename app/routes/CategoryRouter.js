const CategoryRouter = require('express').Router();
const { default: mongoose } = require('mongoose');
const { Category } = require('../models/CategoryModel')

CategoryRouter.post('/categories', async(req, res) => {
    try {
        const category = new Category({
            name: req.body.name,
            description: req.body.description
        })
        const newCategory = await category.save();
        return res.status(201).send(newCategory)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

CategoryRouter.get('/categories', async(req, res) => {
    try {
        const categoryList = await Category.find()
        return res.status(200).send(categoryList)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

CategoryRouter.get('/categories/:id', async(req, res) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Category ")
        };

        const category = await Category.findById(req.params.id)
        return res.status(200).send(category)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

CategoryRouter.put('/categories/:id', async (req, res) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Category ")
        };
        const category = await Category.findByIdAndUpdate(
            req.params.id,
            req.body,
            {
                new: true
            }
        )
        return res.status(200).send(category)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

CategoryRouter.delete('/categories/:id', async(req, res) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Category ")
        };

        await Category.findByIdAndDelete(req.params.id)
        return res.status(200).json({ message: "Delete successfully!" })
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

module.exports = CategoryRouter;
