const PlatformRouter = require('express').Router();
const { default: mongoose } = require('mongoose');
const { Platform } = require('../models/PlatformModel')

PlatformRouter.post('/platforms', async(req, res) => {
    try {
        const platform = new Platform({
            name: req.body.name,
            description: req.body.description
        })
        const newPlatform = await platform.save();
        return res.status(201).send(newPlatform)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

PlatformRouter.get('/platforms', async(req, res) => {
    try {
        const platformList = await Platform.find()
        return res.status(200).send(platformList)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

PlatformRouter.get('/platforms/:id', async(req, res) => {
    try {

        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Platform ")
        };

        const platform = await Platform.findById(req.params.id)
        return res.status(200).send(platform)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

PlatformRouter.put('/platforms/:id', async (req, res) => {
    try {

        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Platform ")
        };

        const platform = await Platform.findByIdAndUpdate(
            req.params.id,
            req.body,
            {
                new: true
            }
        )
        return res.status(200).send(platform)
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

PlatformRouter.delete('/platforms/:id', async(req, res) => {
    try {

        if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json("Invalid Platform ")
        };
        
        await Platform.findByIdAndDelete(req.params.id)
        return res.status(200).json({ message: "Delete successfully!" })
    }
    catch(err) {
        return res.status(500).json({message: `${err.message}`})
    }
});

module.exports = PlatformRouter;
