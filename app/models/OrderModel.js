const mongoose = require("mongoose");

const OrderSchema = mongoose.Schema({
  orderDate: {
    type: Date,
    default: Date.now(),
  },
  shippedDate: {
    type: Date,
  },
  receiver: {
    type: String,
  },
  address: {
    type: String,
  },
  note: {
    type: String,
  },
  orderDetail: [
    {
    
    }
  ],
  cost: {
    type: Number,
    default: 0,
  },
  timeCreated: {
    type: Date,
    default: Date.now(),
  },
  timeUpdated: {
    type: Date,
    default: Date.now(),
  },
});

exports.Order = mongoose.model("Order", OrderSchema);
