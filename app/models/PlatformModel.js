const mongoose = require('mongoose');

const PlatformSchema = mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String,
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
});

exports.Platform = mongoose.model('Platform', PlatformSchema);