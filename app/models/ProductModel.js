const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true,
  },
  description: {
    type: String,
  },
  platform: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Platform",
    required: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  buyPrice: {
    type: Number,
    required: true,
  },
  promotionPrice: {
    type: Number,
    required: true,
  },
  rate: {
    type: Number,
  },
  amount: {
    type: Number,
    default: 50,
  },
  orders: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Order",
    },
  ],
  timeCreated: {
    type: Date,
    default: Date.now(),
  },
  timeUpdated: {
    type: Date,
    default: Date.now(),
  },
});

exports.Product = mongoose.model("Product", ProductSchema);
