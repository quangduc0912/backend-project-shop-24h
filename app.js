const express = require('express');
const cors = require('cors')
const mongoose = require('mongoose');
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors())
const CategoryRouter = require('./app/routes/CategoryRouter');
const PlatformRouter = require('./app/routes/PlatformRouter')
const ProductRouter = require('./app/routes/ProductRouter');
const CustomerRouter = require('./app/routes/CustomerRouter');
const OrderRouter = require('./app/routes/OrderRouter');

app.use('/', CategoryRouter);
app.use('/', PlatformRouter);
app.use('/', ProductRouter);
app.use('/', CustomerRouter);
app.use('/', OrderRouter);

//connect to MongoDB
const uri = 'mongodb+srv://duckq:Tomi3012@cluster0.ysbys1h.mongodb.net/shop'
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
},
 (error) => {
    if (error) throw error;
    console.log('MongoDB connected successfully!')
})

app.listen(process.env.PORT || port);